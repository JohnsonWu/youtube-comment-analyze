# Predict Sentiment

import os
import jieba
import pandas as pd

import torch
import torch.nn as nn
import torch
import torch.nn.functional as F
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from sklearn.feature_extraction.text import CountVectorizer
from tqdm.notebook import tqdm


class Sequences(Dataset):
  def __init__(self, path):
    df = pd.read_csv(path)
    self.vectorizer = CountVectorizer(tokenizer=self.tokenize_zh, stop_words=['。', '，'], max_df=0.99, min_df=0.005)
    self.sequences = self.vectorizer.fit_transform(df.text.tolist())
    self.labels = df.labels.tolist()
    self.token2idx = self.vectorizer.vocabulary_
    self.idx2token = {idx: token for token, idx in self.token2idx.items()}

  def __getitem__(self, i):
    return self.sequences[i, :].toarray(), self.labels[i]
    # return self.sequences[i, :].toarray()

  def __len__(self):
    return self.sequences.shape[0]

  def tokenize_zh(self, text):
    words = jieba.lcut(text)
    return words


class BagOfWordsClassifier(nn.Module):
  def __init__(self, vocab_size, hidden1, hidden2):
    super(BagOfWordsClassifier, self).__init__()
    self.fc1 = nn.Linear(vocab_size, hidden1)
    self.fc2 = nn.Linear(hidden1, hidden2)
    self.fc3 = nn.Linear(hidden2, 1)

  def forward(self, inputs):
    x = F.relu(self.fc1(inputs.squeeze(1).float()))
    x = F.relu(self.fc2(x))
    return self.fc3(x)


def predict_sentiment(model, dataset, device, text):
  model.eval()
  with torch.no_grad():
    test_vector = torch.LongTensor(dataset.vectorizer.transform([text]).toarray())

    output = model(test_vector.to(device))
    prediction = torch.sigmoid(output).item()

    if prediction > 0.5:
      print(f'{prediction:0.3}: Positive sentiment')
    else:
      print(f'{prediction:0.3}: Negative sentiment')


def main():
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

  cwd = os.getcwd()
  DATA_PATH = os.path.join(cwd, './data/youtube_comments2.csv')

  dataset = Sequences(DATA_PATH)
  words_df = pd.DataFrame(dataset.sequences.toarray(),
                          columns=dataset.vectorizer.get_feature_names())
  # print(words_df)

  train_loader = DataLoader(dataset, batch_size=4096)

  model = BagOfWordsClassifier(len(dataset.token2idx), 128, 64).to(device)
  # print(model)

  criterion = nn.BCEWithLogitsLoss()
  optimizer = optim.Adam([p for p in model.parameters() if p.requires_grad], lr=0.001)

  model.train()

  train_losses = []
  for epoch in range(10):
    progress_bar = tqdm(train_loader, leave=False)
    losses = []
    total = 0
    for inputs, target in progress_bar:
      model.zero_grad()

      inputs = inputs.to(device)
      target = target.to(device)

      output = model(inputs)
      loss = criterion(output.squeeze(), target.float())

      loss.backward()

      nn.utils.clip_grad_norm_(model.parameters(), 3)

      optimizer.step()

      progress_bar.set_description(f'Loss: {loss.item():.3f}')

      losses.append(loss.item())
      total += 1

    epoch_loss = sum(losses) / total
    train_losses.append(epoch_loss)

    tqdm.write(f'Epoch #{epoch + 1}\tTrain Loss: {epoch_loss:.3f}')

  test_text = """
              来我主频道或是小号喷我的台独跟恨国党，你们有本事就露脸拍视频举报我啊？难道你们台湾省人没有权利到公安局举报我吗？各个喷子高手，键盘侠！你管我叫林天天还是李爱国。 与你们台独何干？
              """

  predict_sentiment(model, dataset, device, test_text)

  test_text = """
              小粉红被打脸次数(百万)
              """

  predict_sentiment(model, dataset, device, test_text)


if __name__ == "__main__":
  main()
