import os
import pandas as pd
import numpy as np


def main():
  cwd = os.getcwd()
  df = pd.read_csv(os.path.join(cwd, './data/youtube_comments.csv'), index_col=0)

  # 去除 emoji
  df['text'] = df['text'].str.replace('[^\w\s#@/:%.,_-]', '')
  df['text'] = df['text'].str.replace('[\n\r\d+\.+\-\:\_\/]', '')
  # 把只留下 emoji 的留言設為 nan
  df['text'] = np.where(df['text'] == '', np.nan, df['text'])

  df.dropna(inplace=True)
  df = df.reset_index(drop=True)
  
  # 轉換簡體字到繁體字

  df.to_csv(os.path.join(cwd, "./data/youtube_comments2.csv"))


if __name__ == '__main__':
  main()
