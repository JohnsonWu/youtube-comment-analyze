# -*- coding: utf-8 -*-

# Sample Python code for youtube.commentThreads.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/guides/code_samples#python

import os
import pandas as pd
import numpy as np
from dotenv import load_dotenv
# from cemotion import Cemotion
from snownlp import SnowNLP


import googleapiclient.discovery


def append_comments(df_comments, list_author, list_text):
  df_list = pd.DataFrame({"author": list_author, "text": list_text})
  df_comments = df_comments.append(df_list, ignore_index=True)

  return df_comments


def list_of_comments(response):
  list_text = []
  list_author = []
  for index, res in enumerate(response["items"]):
    list_text.append(res["snippet"]["topLevelComment"]["snippet"]["textOriginal"])
    list_author.append(res["snippet"]["topLevelComment"]["snippet"]["authorDisplayName"])

  return list_author, list_text


def main():
  load_dotenv()
  token = os.environ.get("google-token")
  cwd = os.getcwd()
  df_comments = pd.DataFrame(columns=["author", "text"])
  videoId = "YHmJDSxLGik"
  order = "relevance"
  nextPageToken = ""
  # Disable OAuthlib's HTTPS verification when running locally.
  # *DO NOT* leave this option enabled in production.
  os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

  api_service_name = "youtube"
  api_version = "v3"
  DEVELOPER_KEY = token

  youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=DEVELOPER_KEY)

  while True:
    request = youtube.commentThreads().list(part="id, snippet, replies", order=order, pageToken=nextPageToken, videoId=videoId)
    response = request.execute()

    # print(response["items"][0]["snippet"]['totalReplyCount'])
    # print(response["items"][1]["snippet"]['totalReplyCount'])

    list_author, list_text = list_of_comments(response)
    df_comments = append_comments(df_comments, list_author, list_text)
    if "nextPageToken" in response:
      nextPageToken = response["nextPageToken"]
    else:
      # c = Cemotion()
      # df_comments['polarity'] = df_comments['text'].apply(lambda x: c.predict(x))
      df_comments['polarity'] = df_comments['text'].apply(lambda x: SnowNLP(x).sentiments)
      
      
      df_comments['labels'] = np.where(df_comments['polarity'] >= 0.5, 1, 0)
      # df_comments['labels'][df_comments.polarity < 0.5] = 0
      df_comments.to_csv(os.path.join(cwd, "./data/youtube_comments.csv"))
      break


if __name__ == "__main__":
  main()
